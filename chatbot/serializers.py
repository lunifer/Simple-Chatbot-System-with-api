from rest_framework import serializers
from .models import *

class ChatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chats
        fields = (
            'id',
            'input_message',
            'ai_response',
            'created_at'
            )